<?php
/**
 * Created by FixCore.
 * User: Vipo
 * Date: 3/04/2018
 * Time: 9:36 AM
 * All rights reserved 2017 - 2018
 */

namespace blizzcms\encrypts;

class FxEncryptions
{
    public static function passwordClassic($username, $password)
    {
        if (!is_string($username))
            $username = "";

        if (!is_string($password))
            $password = "";

        $sha_pass_hash = sha1(strtoupper($username).':'.strtoupper($password));

        return strtoupper($sha_pass_hash);
    }

    public static function passwordBnet($email, $password)
    {
        return strtoupper(bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($email)).":".strtoupper($password))))))));
    }
}