<?php
/**
 * Created by FixCore.
 * User: Vipo
 * Date: 3/04/2018
 * Time: 7:14 AM
 * All rights reserved 2017 - 2018
 */

include __DIR__ . "/../vendor/autoload.php";

use blizzcms\encrypts\FxEncryptions;

/*
 * Classic Method
 * Classic, Vanilla, Burning Crusade, Wrath of the Lich King, Cataclysm & Mop
 */
echo FxEncryptions::passwordClassic('my_username', 'my_password'); //result with classic method

/*
 * Bnet Method
 * Warlords of Draenor & Legion
 */
echo FxEncryptions::passwordBnet('my_email@fixcore.fx', 'my_password'); //result with bnet method